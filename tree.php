<?php

class Tree
{

    /**
     * @var $map array массив с набором ссылок на узлы дерева
     */
    protected $map = [];

    /**
     * @var $tree array дерево в целом
     */
    protected $tree = [];


    protected function __construct()
    {
        $conn = new PDO("mysql:host=localhost;dbname=pew", 'fidnex');
        $conn->setAttribute(
                PDO::ATTR_ERRMODE,
                PDO::ERRMODE_EXCEPTION
            );

        $stmt = $conn->prepare('SELECT * FROM branches');
        $stmt->execute();

        $branches = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $this->setParents($branches);
        $this->constructTree($branches);
    }

    /**
     * @return object static
     */
    public static function getInstance()
    {
        static $instance;

        if (null === $instance) {
            $instance = new static();
        }

        return $instance;
    }

    /**
     * Заполняет дерево и $this->map
     *
     * @param $branches array
     * @return void
     */
    protected function constructTree(array $branches)
    {

        foreach ($branches as $branch) {
            if (!$branch['pid']) continue;

            if(!isset($this->map[$branch['pid']]['children']))
                $this->map[$branch['pid']]['children'] = [];

            $this->map[$branch['pid']]['children'] = array_merge(
                $this->map[$branch['pid']]['children'],
                [$branch]
            );

            $this->map[$branch['id']] = &$this->map[$branch['pid']]['children'][
                count($this->map[$branch['pid']]['children']) - 1
            ];
        }

    }

    /**
     * Устанавливает родителей
     *
     * @param $branches;
     * @return void
     */
    protected function setParents(array $branches)
    {
        foreach ($branches as $branch) {
            if($branch['pid']) continue;

            $this->map[$branch['id']] = $branch;
            $this->tree[$branch['id']] = &$this->map[$branch['id']];
        }
    }

    /**
     * Отдает сформированное дерево
     *
     * @return array
     */
    public function getTree()
    {
        return $this->tree;
    }

    /**
     * Отдает ветку с ее детьми
     *
     * @param $id int айди ветки
     * @return mixed
     */
    public function getBranch($id)
    {
        if(isset($this->map[$id]))
            return $this->map[$id];

        return null;
    }

    /**
     * Проверяет принадлежность ветки к другой
     *
     * @param $id int айди проверяемой ветки
     * @param $parentId int айди ветки родителя
     * @return boolean
     */
    public function isParent($id, $parentId)
    {
        if(!$this->map[$id]['pid'])
            return false;

        if($this->map[$id]['pid'] == $parentId)
            return true;

        return $this->isParent($this->map[$id]['pid'], $parentId);
    }

}

$tree = Tree::getInstance();

print_r($tree->getTree());
print_r($tree->getBranch(6));

var_dump($tree->isParent(6, 2)); // true
var_dump($tree->isParent(6, 1)); // false